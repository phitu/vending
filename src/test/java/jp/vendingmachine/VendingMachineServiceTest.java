package jp.vendingmachine;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;

public class VendingMachineServiceTest {

	VendingMachineService vendingMachineService = new VendingMachineService();
		
	@Test
	public void decreaseMoneyAmountIfMoreThanQuantity() {
		vendingMachineService.getChangeList().add(new ChangeAvailability(Money.DOLLAR, 1));
		
		boolean deQty = vendingMachineService.decreaseMoneyAmt(Money.DOLLAR);
		assertThat(deQty, is(true));
		assertThat(vendingMachineService.getChangeList().get(0).getChangeQuantity(), is(0));
	}

	@Test
	public void decreaseMoneyAmountIfLessThanQuantity() {
		vendingMachineService.getChangeList().add(new ChangeAvailability(Money.DOLLAR, 0));
		boolean deQty = vendingMachineService.decreaseMoneyAmt(Money.DOLLAR);
		assertThat(deQty, is(false));
	}

	@Test
	public void increaseMoneyAmount() {
		vendingMachineService.getChangeList().add(new ChangeAvailability(Money.DOLLAR, 0));
		vendingMachineService.increaseMoneyAmt(Money.DOLLAR);
		
		assertThat(vendingMachineService.getChangeList().get(0).getChangeQuantity(), is(1));
	}
}
