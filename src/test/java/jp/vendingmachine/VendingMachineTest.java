package jp.vendingmachine;

import static org.junit.Assert.assertThat;

import java.util.stream.Collectors;

import static org.hamcrest.core.Is.is;

import org.junit.Test;

public class VendingMachineTest {

	VendingMachineService vendingMachineService = new VendingMachineService();
	VendingMachine vendingMachine = new VendingMachine(vendingMachineService);
	
	@Test
	public void getItemReducesItemCount() {
		vendingMachineService.getChangeList().add(new ChangeAvailability(Money.DOLLAR, 0));
		vendingMachineService.getChangeList().add(new ChangeAvailability(Money.QUARTER, 0));
		vendingMachineService.getChangeList().add(new ChangeAvailability(Money.NICKEL, 0));
		vendingMachineService.getChangeList().add(new ChangeAvailability(Money.DIME, 0));
		
		vendingMachine.insertMoney(Money.QUARTER);
		vendingMachine.insertMoney(Money.QUARTER);
		vendingMachine.insertMoney(Money.DIME);
		vendingMachine.insertMoney(Money.NICKEL);
		Item buyItem = new ItemA();
		buyItem.setQuantity(6);
		boolean itemExists = vendingMachine.getVendingMachineItem(buyItem);
		
		assertThat(buyItem.getQuantity(), is(5));
		assertThat(itemExists, is(true));
	}
	
	@Test
	public void getItemReturnsFalseIfMoneyNotAvailable() {
		Item buyItem = new ItemA();
		buyItem.setQuantity(6);
		boolean itemExists = vendingMachine.getVendingMachineItem(buyItem);
		
		assertThat(itemExists, is(false));
	}
	
	@Test
	public void getItemReducesMoney() {
		vendingMachineService.getChangeList().add(new ChangeAvailability(Money.DOLLAR, 0));
		vendingMachineService.getChangeList().add(new ChangeAvailability(Money.QUARTER, 0));
		vendingMachineService.getChangeList().add(new ChangeAvailability(Money.NICKEL, 0));
		vendingMachineService.getChangeList().add(new ChangeAvailability(Money.DIME, 1));
		
		vendingMachine.insertMoney(Money.QUARTER);
		vendingMachine.insertMoney(Money.QUARTER);
		vendingMachine.insertMoney(Money.QUARTER);
		
		
		Item buyItem = new ItemA();
		buyItem.setQuantity(6);
		boolean itemExists = vendingMachine.getVendingMachineItem(buyItem);
	
		assertThat(itemExists, is(true));
		assertThat(vendingMachine.getChange().size(), is(1));
		assertThat(vendingMachine.getTotalMoney(), is(0.1));
	}
	
	@Test
	public void getItemReturnsNickelChangeIfDimeNotPresent() {
		vendingMachineService.getChangeList().add(new ChangeAvailability(Money.DOLLAR, 0));
		vendingMachineService.getChangeList().add(new ChangeAvailability(Money.QUARTER, 0));
		vendingMachineService.getChangeList().add(new ChangeAvailability(Money.NICKEL, 4));
		vendingMachineService.getChangeList().add(new ChangeAvailability(Money.DIME, 0));
		
		vendingMachine.insertMoney(Money.QUARTER);
		vendingMachine.insertMoney(Money.QUARTER);
		vendingMachine.insertMoney(Money.QUARTER);
		
		
		Item buyItem = new ItemA();
		buyItem.setQuantity(6);
		boolean itemExists = vendingMachine.getVendingMachineItem(buyItem);
	
		assertThat(itemExists, is(true));
		assertThat(vendingMachine.getChange().size(), is(2));
		assertThat(vendingMachine.getTotalMoney(), is(0.1));
		assertThat(vendingMachine.getChange().get(0), is(Money.NICKEL));
	}
	
	@Test
	public void getItemReturnsNoChangeIfNoCoinsAvailableInMachine() {
		vendingMachineService.getChangeList().add(new ChangeAvailability(Money.DOLLAR, 0));
		vendingMachineService.getChangeList().add(new ChangeAvailability(Money.QUARTER, 0));
		vendingMachineService.getChangeList().add(new ChangeAvailability(Money.NICKEL, 0));
		vendingMachineService.getChangeList().add(new ChangeAvailability(Money.DIME, 0));
		
		vendingMachine.insertMoney(Money.QUARTER);
		vendingMachine.insertMoney(Money.QUARTER);
		vendingMachine.insertMoney(Money.QUARTER);
		
		
		Item buyItem = new ItemA();
		buyItem.setQuantity(6);
		boolean itemExists = vendingMachine.getVendingMachineItem(buyItem);
	
		assertThat(itemExists, is(true));
		assertThat(vendingMachine.getChange().size(), is(0));
	}
	
	@Test
	public void itemBReturnsNoChange() {
		vendingMachineService.getChangeList().add(new ChangeAvailability(Money.DOLLAR, 0));
		vendingMachineService.getChangeList().add(new ChangeAvailability(Money.QUARTER, 0));
		vendingMachineService.getChangeList().add(new ChangeAvailability(Money.NICKEL, 0));
		vendingMachineService.getChangeList().add(new ChangeAvailability(Money.DIME, 0));
		
		vendingMachine.insertMoney(Money.DOLLAR);		
		
		Item buyItem = new ItemB();
		buyItem.setQuantity(6);
		boolean itemExists = vendingMachine.getVendingMachineItem(buyItem);
	
		assertThat(itemExists, is(true));
		assertThat(vendingMachine.getChange().size(), is(0));
	}
	
	@Test
	public void itemDoesNotExistReturnsFalse() {
		vendingMachineService.getChangeList().add(new ChangeAvailability(Money.DOLLAR, 0));
		vendingMachineService.getChangeList().add(new ChangeAvailability(Money.QUARTER, 0));
		vendingMachineService.getChangeList().add(new ChangeAvailability(Money.NICKEL, 0));
		vendingMachineService.getChangeList().add(new ChangeAvailability(Money.DIME, 0));
		
		vendingMachine.insertMoney(Money.DOLLAR);		
		
		Item buyItem = new ItemB();
		buyItem.setQuantity(0);
		boolean itemExists = vendingMachine.getVendingMachineItem(buyItem);
	
		assertThat(itemExists, is(false));
	}
	
	@Test
	public void boughtItemIncreaseChangeCount() {
		vendingMachineService.getChangeList().add(new ChangeAvailability(Money.DOLLAR, 0));
		vendingMachineService.getChangeList().add(new ChangeAvailability(Money.QUARTER, 0));
		vendingMachineService.getChangeList().add(new ChangeAvailability(Money.NICKEL, 0));
		vendingMachineService.getChangeList().add(new ChangeAvailability(Money.DIME, 0));
		
		vendingMachine.insertMoney(Money.DOLLAR);		
		
		Item buyItem = new ItemB();
		buyItem.setQuantity(1);
		vendingMachine.getVendingMachineItem(buyItem);		

		ChangeAvailability change = vendingMachineService.getChangeList().stream()
				.filter(a->a.getMoney().name().equals(Money.DOLLAR.name())).collect(Collectors.toList()).get(0);		
		
		assertThat(change.getChangeQuantity(), is(1));
	}
	
	@Test
	public void coinReturnClearsAll() {
		vendingMachineService.getChangeList().add(new ChangeAvailability(Money.DOLLAR, 0));
		vendingMachineService.getChangeList().add(new ChangeAvailability(Money.QUARTER, 0));
		vendingMachineService.getChangeList().add(new ChangeAvailability(Money.NICKEL, 0));
		vendingMachineService.getChangeList().add(new ChangeAvailability(Money.DIME, 0));
		
		vendingMachine.insertMoney(Money.QUARTER);	
		vendingMachine.insertMoney(Money.DOLLAR);		
		vendingMachine.insertMoney(Money.NICKEL);		
		vendingMachine.insertMoney(Money.DIME);			
		
		vendingMachine.coinReturn();

		ChangeAvailability changeQ = vendingMachineService.getChangeList().stream()
				.filter(a->a.getMoney().name().equals(Money.QUARTER.name())).collect(Collectors.toList()).get(0);
		ChangeAvailability changeDi = vendingMachineService.getChangeList().stream()
				.filter(a->a.getMoney().name().equals(Money.DIME.name())).collect(Collectors.toList()).get(0);	
		ChangeAvailability changeNi = vendingMachineService.getChangeList().stream()
				.filter(a->a.getMoney().name().equals(Money.NICKEL.name())).collect(Collectors.toList()).get(0);	
		ChangeAvailability changeDo = vendingMachineService.getChangeList().stream()
				.filter(a->a.getMoney().name().equals(Money.DOLLAR.name())).collect(Collectors.toList()).get(0);			
		
		assertThat(changeQ.getChangeQuantity(), is(0));	
		assertThat(changeDi.getChangeQuantity(), is(0));	
		assertThat(changeNi.getChangeQuantity(), is(0));	
		assertThat(changeDo.getChangeQuantity(), is(0));
		
		assertThat(vendingMachine.getTotalMoney(), is(0.0));
	}
}
