package jp.vendingmachine;

public class ChangeAvailability {

	private Money money;
	
	private int changeQuantity;
	
	public ChangeAvailability(Money m, int quantity) {
		setMoney(m);
		setChangeQuantity(quantity);
	}

	public Money getMoney() {
		return money;
	}

	public void setMoney(Money money) {
		this.money = money;
	}

	public int getChangeQuantity() {
		return changeQuantity;
	}

	public void setChangeQuantity(int changeQuantity) {
		this.changeQuantity = changeQuantity;
	}
}
