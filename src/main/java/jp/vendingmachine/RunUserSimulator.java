package jp.vendingmachine;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

public class RunUserSimulator {
	
	private VendingMachineService vendingMachineService = new VendingMachineService();
	private VendingMachine vendingMachine;
	private BufferedReader c = new BufferedReader(new InputStreamReader(System.in));
	private Item item1;
	private Item item2;
	private Item item3;
	
	public RunUserSimulator() {
		vendingMachine = new VendingMachine(vendingMachineService);
		setVendingMachineChangeValues();
		setItemQuantity();
	}
	
	private void setVendingMachineChangeValues() {
		ChangeAvailability dollarQuantity = new ChangeAvailability(Money.DOLLAR, 6);
		ChangeAvailability dimQuantity = new ChangeAvailability(Money.DIME, 6);
		ChangeAvailability quarterQuantity = new ChangeAvailability(Money.QUARTER, 6);
		ChangeAvailability nickelQuantity = new ChangeAvailability(Money.NICKEL, 6);

		vendingMachineService.getChangeList().add(dollarQuantity);
		vendingMachineService.getChangeList().add(dimQuantity);
		vendingMachineService.getChangeList().add(quarterQuantity);
		vendingMachineService.getChangeList().add(nickelQuantity);
	}
	
	private void setItemQuantity() {
		item1 = new ItemA();
		item1.setQuantity(4);
		item2 = new ItemB();
		item2.setQuantity(2);
		item3 = new ItemC();
		item3.setQuantity(0);
	}
	
	private void insertCoins() throws IOException {

		System.out.println("Enter 'Dollar', 'Q', 'D' ,'N'");
		System.out.println("Enter 1 to select item");
		
		String coin = c.readLine();
		switch(coin) {
			case "Dollar" : vendingMachine.insertMoney(Money.DOLLAR);	
				System.out.println(vendingMachine.getTotalMoney() + " inserted");
				insertCoins();
				break;
			case "Q" : vendingMachine.insertMoney(Money.QUARTER);	
				System.out.println(vendingMachine.getTotalMoney() + " inserted");
				insertCoins();
				break;
			case "D" : vendingMachine.insertMoney(Money.DIME);	
				System.out.println(vendingMachine.getTotalMoney() + " inserted");
				insertCoins();
				break;
			case "N" : vendingMachine.insertMoney(Money.NICKEL);	
				System.out.println(vendingMachine.getTotalMoney() + " inserted");
				insertCoins();
				break;
			case "1" : selectItem();
				break;
			default : System.out.println("Command not recognised");
				insertCoins();
		}
	}
	
	private void selectItem() throws IOException {

		System.out.println("Enter 1 for item 1");
		System.out.println("Enter 2 for item 2");
		System.out.println("Enter 3 for item 3");
		System.out.println("Enter 4 to return coins");
		String itemMenu = c.readLine();
		boolean itemReceived = true;
		switch(itemMenu) {
			case "1" : itemReceived = vendingMachine.getVendingMachineItem(item1);
				itemCheck(itemReceived);
				break;
			case "2" : itemReceived = vendingMachine.getVendingMachineItem(item2);
				itemCheck(itemReceived);
				break;
			case "3" : itemReceived = vendingMachine.getVendingMachineItem(item3);
				itemCheck(itemReceived);
				break;
			case "4" : vendingMachine.coinReturn();
				itemCheck(itemReceived);
				break;
			default : System.out.println("Command not recognised");
				selectItem();
				break;
		}
	}

	private void itemCheck(boolean itemReceived) throws IOException {
		if(itemReceived == false) {
			System.out.println("Error with selecting item");
			selectItem();
		}
		else {
			receiveChange();
		}
	}

	private void receiveChange() {
		List<Money> change = vendingMachine.getChange();
		if(change.size() > 0) {
			for(Money m : change) {
				System.out.println(m + " returned");
			}
		}
	}
	
	public void displayMenu() throws IOException {
		System.out.println("Enter 1 to insert coins");
		System.out.println("Enter 2 to select item");
		System.out.println("Enter 3 to return coins");
		String menu = c.readLine();
		switch(menu) {
			case "1" : insertCoins();
				break;
			case "2" : selectItem();
				break;
			case "3" : vendingMachine.coinReturn();
				break;
			default : System.out.println("Command not recognised");
				displayMenu();
		}
	}
	
	public static void main(String[] args) {
		RunUserSimulator app = new RunUserSimulator();
		try {
			app.displayMenu();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
