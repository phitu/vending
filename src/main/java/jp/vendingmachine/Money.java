package jp.vendingmachine;

public enum Money {

	NICKEL(0.05), DIME(0.10), QUARTER(0.25), DOLLAR(1.00);
	
	private final double moneyCode;
	
	private Money(double money) {
		this.moneyCode = money;
	}

	public double getMoney() {
		return moneyCode;
	}
}
