package jp.vendingmachine;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class VendingMachineService {

	private List<ChangeAvailability> changeList = new ArrayList<>();
	
	public List<ChangeAvailability> getChangeList() {
		return changeList;
	}	
	
	public boolean decreaseMoneyAmt(Money m) {
		ChangeAvailability change = changeList.stream().filter(a->a.getMoney().name().equals(m.name())).collect(Collectors.toList()).get(0);
		if(change.getChangeQuantity() > 0) {
			change.setChangeQuantity(change.getChangeQuantity()-1);
			return true;
		}
		return false;
	}
	
	public void increaseMoneyAmt(Money m) {
		ChangeAvailability change = changeList.stream().filter(a->a.getMoney().name().equals(m.name())).collect(Collectors.toList()).get(0);		
		
		change.setChangeQuantity(change.getChangeQuantity()+1);
			
	}
	
	public void setItemQuantity(Item item, int quantity) {
		item.setQuantity(quantity);
	}
		
}
