package jp.vendingmachine;

import java.util.ArrayList;
import java.util.List;

public class VendingMachine {

	private VendingMachineService vendingMachineService;
	private List<Money> insertedMoney = new ArrayList<>();
	private List<Money> availableChange = new ArrayList<>();
	private double totalMoney;
	
	public VendingMachine(VendingMachineService vendingMachineService) {
		this.vendingMachineService = vendingMachineService;
	}
	
	public void coinReturn() {
		for(Money m : insertedMoney) {
			System.out.println(m.name() + " returned");
			vendingMachineService.decreaseMoneyAmt(m);
		}
		insertedMoney.clear();
		calculateTotalMoney();
	}
	
	public void insertMoney(Money m) {
		insertedMoney.add(m);
		vendingMachineService.increaseMoneyAmt(m);
		calculateTotalMoney();
	}
	
	public boolean getVendingMachineItem(Item item) {
		if(calculateTotalMoney() >= item.getPrice() && item.getQuantity() > 0) {
			item.setQuantity(item.getQuantity()-1);
			reduceMoney(item);
			return true;
		}
		return false;
	}
	
	private double calculateTotalMoney() {
		double total = 0;
		for(Money m : insertedMoney) {
			total += m.getMoney();
		}
		setTotalMoney(Math.round(total * 100.0) / 100.0);
		return total;
	}
	
	public List<Money> getChange() {
		double totalChange = getTotalMoney();
		totalChange = calcChange(totalChange, Money.DOLLAR);
		totalChange = calcChange(totalChange, Money.QUARTER);
		totalChange = calcChange(totalChange, Money.DIME);
		totalChange = calcChange(totalChange, Money.NICKEL);
		return availableChange;
	}

	private double calcChange(double totalChange, Money m) {
		while(totalChange >= m.getMoney() && vendingMachineService.decreaseMoneyAmt(m)) {
			availableChange.add(m);
			totalChange -= m.getMoney();
			
		}
		return totalChange;
	}
	
	private void reduceMoney(Item item) {
		setTotalMoney(Math.round((getTotalMoney() - item.getPrice()) * 100.0) / 100.0);
	}

	public double getTotalMoney() {
		return totalMoney;
	}

	public void setTotalMoney(double totalMoney) {
		this.totalMoney = totalMoney;
	}
}
